package com.fq.web.domain;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author jifang.
 * @since 2016/5/23 10:41.
 */
public class SScoreVO {

    // 课程信息, 为客户端展示所用
    @JSONField(name = "c_name")
    private String cName;

    private Integer credit;

    @JSONField(name = "c_id")
    private Integer cId;

    // 课程成绩
    private Double number;

    // 教师信息, 为给老师留言使用
    @JSONField(name = "t_name")
    private String tName;

    @JSONField(name = "t_id")
    private Integer tId;

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }
}
