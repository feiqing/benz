package com.fq.web.domain;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author jifang.
 * @since 2016/5/23 9:41.
 */
public class UserVO {

    @JSONField(name = "u_id")
    private Integer id;

    private String name;

    private String password;

    public UserVO() {
    }

    public UserVO(Integer id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
