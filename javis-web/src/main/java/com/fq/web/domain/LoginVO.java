package com.fq.web.domain;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * @author jifang.
 * @since 2016/5/22 20:10.
 */
public class LoginVO implements Serializable {

    @JSONField(name = "is_login")
    private Boolean isLogin;

    private Integer id;

    public Boolean getIsLogin() {
        return isLogin;
    }

    public void setIsLogin(Boolean isLogin) {
        this.isLogin = isLogin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
