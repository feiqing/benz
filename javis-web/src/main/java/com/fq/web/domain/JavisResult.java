package com.fq.web.domain;

/**
 * @author jifang
 * @since 16/2/29 下午1:32.
 */
public class JavisResult<T> {

    private T data;

    private Integer status = 0;

    private String description = "";

    public JavisResult() {
    }

    public JavisResult(T data) {
        this.data = data;
    }

    public JavisResult(T data, Integer status) {
        this.data = data;
        this.status = status;
    }

    public JavisResult(Integer status, String description) {
        this.status = status;
        this.description = description;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
