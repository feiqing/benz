package com.fq.web.domain;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author jifang.
 * @since 2016/5/24 9:14.
 */
public class TScoreVO {

    @JSONField(name = "u_id")
    private Integer uId;

    @JSONField(name = "u_name")
    private String uName;

    @JSONField(name = "number")
    private Double number;

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }
}
