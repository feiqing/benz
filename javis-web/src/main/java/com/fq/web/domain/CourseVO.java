package com.fq.web.domain;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * @author jifang
 * @since 16/2/29 上午10:20.
 */
public class CourseVO implements Serializable {

    private static final long serialVersionUID = 2625655110584180267L;

    // 课程ID
    private Integer id;

    // 课程名
    private String name;

    // 课程学分
    private Integer credit;

    @JSONField(name = "t_id")
    private Integer tId;

    @JSONField(name = "teacher_name")
    private String tName;

    public CourseVO() {
    }

    public CourseVO(Integer id, String name, Integer credit) {
        this.id = id;
        this.name = name;
        this.credit = credit;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }
}
