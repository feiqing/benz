package com.fq.web.controller;

import com.fq.client.service.ChooseService;
import com.fq.web.domain.JavisResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jifang
 * @since 16/3/1 下午2:22.
 */
@Controller
@RequestMapping("/choose")
public class ChooseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChooseController.class);

    @Autowired
    private ChooseService service;

    @ResponseBody
    @RequestMapping("/is_added.do")
    public JavisResult<Boolean> isAdded(@RequestParam("u_id") Integer uId,
                                        @RequestParam("c_id") Integer cId) {
        return new JavisResult<>(service.isAdded(uId, cId));
    }


    @ResponseBody
    @RequestMapping("/add.do")
    public JavisResult<Integer> add(@RequestParam("u_id") Integer uId,
                                    @RequestParam("c_id") Integer cId) {
        Integer id;
        try {
            id = service.addChoose(uId, cId);
        } catch (Exception e) {
            LOGGER.error("/choose/add.do error: ", e);
            return new JavisResult<>(-1, e.getLocalizedMessage());
        }
        return new JavisResult<>(id);
    }

    @ResponseBody
    @RequestMapping("/cancel.do")
    public JavisResult<Void> cancel(@RequestParam("u_id") Integer uId,
                                    @RequestParam("c_id") Integer cId) {
        try {
            service.cancelChoose(uId, cId);
        } catch (Exception e) {
            LOGGER.error("/choose/cancel.do error: ", e);
            return new JavisResult<>(-1, e.getLocalizedMessage());
        }
        return new JavisResult<>();
    }

    @ResponseBody
    @RequestMapping("/update_score.do")
    public JavisResult<Void> cancel(Double number,
                                    @RequestParam("u_id") Integer uId,
                                    @RequestParam("c_id") Integer cId) {
        service.updateScore(uId, cId, number);
        return new JavisResult<>();
    }
}
