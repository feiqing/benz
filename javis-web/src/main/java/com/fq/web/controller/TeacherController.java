package com.fq.web.controller;

import com.fq.client.service.TeacherService;
import com.fq.web.domain.JavisResult;
import com.fq.web.domain.LoginVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jifang
 * @since 15/12/16 下午7:57.
 */
@Controller
@RequestMapping("/teacher")
public class TeacherController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherController.class);

    @Autowired
    private TeacherService service;

    @ResponseBody
    @RequestMapping("/register.do")
    public JavisResult<Boolean> register(String name, String password) {
        try {
            service.register(name, password);
            return new JavisResult<>(true);
        } catch (Exception e) {
            LOGGER.error("register.do error", e);
        }
        return new JavisResult<>(false);
    }

    @ResponseBody
    @RequestMapping("/login.do")
    public JavisResult<LoginVO> login(String name, String password) {
        Integer id = service.login(name, password);
        LoginVO result = new LoginVO();
        if (id != null) {
            result.setId(id);
            result.setIsLogin(true);
        } else {
            result.setIsLogin(false);
        }
        return new JavisResult<>(result);
    }
}