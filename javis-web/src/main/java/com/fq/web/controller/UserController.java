package com.fq.web.controller;

import com.fq.client.service.UserService;
import com.fq.web.domain.JavisResult;
import com.fq.web.domain.LoginVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jifang
 * @since 15/12/16 下午7:57.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service;

    @ResponseBody
    @RequestMapping("/login.do")
    public JavisResult<LoginVO> login(Integer id, String password) {
        id = service.login(id, password);
        LoginVO result = new LoginVO();
        if (id == null) {
            result.setIsLogin(false);
        } else {
            result.setIsLogin(true);
            result.setId(id);
        }
        return new JavisResult<>(result);
    }

    @ResponseBody
    @RequestMapping("/update_password.do")
    public JavisResult<Boolean> updatePassword(Integer id,
                                               @RequestParam(value = "old_password") String oldPassword,
                                               @RequestParam(value = "new_password") String newPassword
    ) {
        return new JavisResult<>(service.updatePasswordById(id, oldPassword, newPassword));
    }
}