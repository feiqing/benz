package com.fq.web.controller;

import com.fq.client.service.MessageService;
import com.fq.web.domain.JavisResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author jifang
 * @since 16/3/1 上午9:51.
 */
@Controller
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService service;

    @ResponseBody
    @RequestMapping("/send.do")
    public JavisResult<Void> send(@RequestParam("u_id") String uId,
                                  @RequestParam("t_id") String tId,
                                  String message) {
        service.sendMsg(message, uId, tId);
        return new JavisResult<>();
    }

    @ResponseBody
    @RequestMapping("/recv.do")
    public JavisResult<Map<String, String>> recv(@RequestParam("t_id") String tId) {
        return new JavisResult<>(service.recvMsg(tId));
    }

    @ResponseBody
    @RequestMapping("/new_count.do")
    public JavisResult<Long> newCount(@RequestParam("t_id") String tId) {
        return new JavisResult<>(service.getNewMsgCount(tId));
    }
}
