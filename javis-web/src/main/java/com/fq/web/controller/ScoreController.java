package com.fq.web.controller;

import com.fq.client.domain.ScoreDO;
import com.fq.client.service.ScoreService;
import com.fq.web.convertor.ScoreConverter;
import com.fq.web.domain.JavisResult;
import com.fq.web.domain.SScoreVO;
import com.fq.web.domain.TScoreVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author jifang.
 * @since 2016/5/24 8:39.
 */
@Controller
@RequestMapping("/score")
public class ScoreController {

    @Autowired
    private ScoreService service;

    /**
     * 教师端
     **/
    @ResponseBody
    @RequestMapping("/student_list.do")
    public JavisResult<List<TScoreVO>> studentList(@RequestParam("c_id") Integer cId) {
        List<ScoreDO> students = service.getStudentList(cId);
        return new JavisResult<>(ScoreConverter.tConvert(students));
    }

    @ResponseBody
    @RequestMapping("/student_top_n.do")
    public JavisResult<List<TScoreVO>> studentTopN(@RequestParam("c_id") Integer cId,
                                                   Integer n) {
        List<ScoreDO> students = service.getStudentTopN(cId, n);
        return new JavisResult<>(ScoreConverter.tConvert(students));
    }

    @ResponseBody
    @RequestMapping("/student_un_passed.do")
    public JavisResult<List<TScoreVO>> studentUnPassed(@RequestParam("c_id") Integer cId) {
        List<ScoreDO> students = service.getStudentUnPassed(cId);
        return new JavisResult<>(ScoreConverter.tConvert(students));
    }

    /**
     * 学生端
     **/
    @ResponseBody
    @RequestMapping("/score_list.do")
    public JavisResult<List<SScoreVO>> scoreList(@RequestParam("u_id") Integer uId) {
        List<ScoreDO> scores = service.getScoreList(uId);
        return new JavisResult<>(ScoreConverter.sConvert(scores));
    }

    @ResponseBody
    @RequestMapping("/score_top_n.do")
    public JavisResult<List<SScoreVO>> scoreTopN(@RequestParam("u_id") Integer uId,
                                                 Integer n) {
        List<ScoreDO> scores = service.getScoreTopN(uId, n);
        return new JavisResult<>(ScoreConverter.sConvert(scores));
    }

    @ResponseBody
    @RequestMapping("/score_un_passed.do")
    public JavisResult<List<SScoreVO>> scoreUnPassed(@RequestParam("u_id") Integer uId) {
        List<ScoreDO> scores = service.getScoreUnPassed(uId);
        return new JavisResult<>(ScoreConverter.sConvert(scores));
    }

    @ResponseBody
    @RequestMapping("/score_total.do")
    public JavisResult<Double> scoreTotal(@RequestParam("u_id") Integer uId) {
        Double total = service.getScoreTotal(uId);
        return new JavisResult<>(total);
    }
}
