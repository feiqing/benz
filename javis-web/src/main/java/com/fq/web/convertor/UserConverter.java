package com.fq.web.convertor;

import com.fq.client.domain.UserDO;
import com.fq.web.domain.UserVO;
import com.google.common.base.Converter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jifang.
 * @since 2016/5/22 21:33.
 */
public class UserConverter {

    public static final Converter<UserDO, UserVO> converter = new Converter<UserDO, UserVO>() {
        @Override
        protected UserVO doForward(UserDO userDO) {
            return new UserVO(userDO.getId(), userDO.getName(), null);
        }

        @Override
        protected UserDO doBackward(UserVO userVO) {
            return null;
        }
    };

    public static List<UserVO> convert(List<UserDO> inputs) {
        List<UserVO> outputs = new ArrayList<>();
        if (inputs != null) {
            for (UserDO input : inputs) {
                outputs.add(converter.convert(input));
            }
        }
        return outputs;
    }
}
