package com.fq.web.convertor;

import com.fq.client.domain.CourseDO;
import com.fq.web.domain.CourseVO;
import com.google.common.base.Converter;

/**
 * @author jifang.
 * @since 2016/5/22 21:33.
 */
public class CourseConverter {
    public static final Converter<CourseDO, CourseVO> converter = new Converter<CourseDO, CourseVO>() {
        @Override
        protected CourseVO doForward(CourseDO input) {
            CourseVO output = new CourseVO(input.getId(), input.getName(), input.getCredit());
            output.settId(input.gettId());
            output.settName(input.getTeacher().getName());
            return output;
        }

        @Override
        protected CourseDO doBackward(CourseVO courseVO) {
            return null;
        }
    };
}
