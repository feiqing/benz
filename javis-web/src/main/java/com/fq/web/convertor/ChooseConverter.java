package com.fq.web.convertor;

import com.fq.client.domain.ChooseDO;
import com.fq.web.domain.SScoreVO;
import com.google.common.base.Converter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jifang.
 * @since 2016/5/23 10:57.
 */
public class ChooseConverter {

    private static final Converter<ChooseDO, SScoreVO> converter = new Converter<ChooseDO, SScoreVO>() {
        @Override
        protected SScoreVO doForward(ChooseDO input) {
            SScoreVO output = new SScoreVO();
            output.setNumber(input.getNumber());
            output.setcId(input.getcId());
            output.setcName(input.getCourse().getName());
            output.settName(input.getCourse().getTeacher().getName());
            output.setCredit(input.getCourse().getCredit());
            output.settId(input.getCourse().getTeacher().getId());
            return output;
        }

        @Override
        protected ChooseDO doBackward(SScoreVO SScoreVO) {
            return null;
        }
    };

    public static List<SScoreVO> convert(List<ChooseDO> inputs) {
        List<SScoreVO> outputs = new ArrayList<>();
        if (inputs != null) {
            for (ChooseDO input : inputs) {
                outputs.add(converter.convert(input));
            }
        }
        return outputs;
    }
}
