package com.fq.web.convertor;

import com.fq.client.domain.ScoreDO;
import com.fq.web.domain.SScoreVO;
import com.fq.web.domain.TScoreVO;
import com.google.common.base.Converter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jifang.
 * @since 2016/5/24 9:19.
 */
public class ScoreConverter {

    private static final Converter<ScoreDO, TScoreVO> tConverter = new Converter<ScoreDO, TScoreVO>() {

        @Override
        protected TScoreVO doForward(ScoreDO input) {
            TScoreVO output = new TScoreVO();
            output.setNumber(input.getNumber());
            output.setuId(input.getUser().getId());
            output.setuName(input.getUser().getName());
            return output;
        }

        @Override
        protected ScoreDO doBackward(TScoreVO TScoreVO) {
            return null;
        }
    };

    private static final Converter<ScoreDO, SScoreVO> sConverter = new Converter<ScoreDO, SScoreVO>() {
        @Override
        protected SScoreVO doForward(ScoreDO input) {
            SScoreVO output = new SScoreVO();
            output.setNumber(input.getNumber());
            output.setcId(input.getcId());
            output.setcName(input.getCourse().getName());
            output.settName(input.getCourse().getTeacher().getName());
            output.setCredit(input.getCourse().getCredit());
            output.settId(input.getCourse().getTeacher().getId());
            return output;
        }

        @Override
        protected ScoreDO doBackward(SScoreVO sScoreVO) {
            return null;
        }
    };

    // 教师端
    public static List<TScoreVO> tConvert(List<ScoreDO> inputs) {
        List<TScoreVO> outputs = new ArrayList<>();
        if (inputs != null) {
            for (ScoreDO input : inputs) {
                outputs.add(tConverter.convert(input));
            }
        }

        return outputs;
    }

    // 学生端
    public static List<SScoreVO> sConvert(List<ScoreDO> inputs) {
        List<SScoreVO> outputs = new ArrayList<>();
        if (inputs != null) {
            for (ScoreDO input : inputs) {
                outputs.add(sConverter.convert(input));
            }
        }

        return outputs;
    }
}
