<!DOCTYPE HTML>
<html>
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);
    function hideURLbar() {
        window.scrollTo(0, 1);
    } </script>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css'/>
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!----webfonts--->
    <link href='http://fonts.useso.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
    <!---//webfonts--->
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</head>
<body id="login">
<div class="login-logo">
    <a href="index.jsp"><img src="images/logo.png" alt=""/></a>
</div>
<h2 class="form-heading">login</h2>

<div class="app-cam">
    <form action="${pageContext.request.contextPath}/admin/login.do" method="post">
        <input type="text" name="name" class="text" value="Admin name" onfocus="this.value = '';"
               onblur="if (this.value == '') {this.value = 'E-mail address';}">
        <input type="password" name="password" value="Password" onfocus="this.value = '';"
               onblur="if (this.value == '') {this.value = 'Password';}">

        <div class="submit"><input type="submit" onclick="myFunction()" value="Login"></div>
        <ul class="new">
            <div class="clearfix"></div>
        </ul>
    </form>
</div>
</body>
</html>
