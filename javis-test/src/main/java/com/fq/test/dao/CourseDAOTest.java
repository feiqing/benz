package com.fq.test.dao;

import com.fq.client.domain.CourseDO;
import com.fq.core.dao.CourseDAO;
import com.fq.test.TestBase;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author jifang
 * @since 16/2/29 下午7:14.
 */
public class CourseDAOTest extends TestBase {

    @Autowired
    private CourseDAO dao;

    @org.junit.Test
    public void testInsertCourse() throws Exception {
//        dao.insertCourse("算法设计与分析", 4, 1);
//        dao.insertCourse("编译原理", 5, 1);
//        dao.insertCourse("数据结构", 6, 9);
//        dao.insertCourse("高等数学(上)", 2, 4);
//        dao.insertCourse("高等数学(下)", 2, 4);
//        dao.insertCourse("线性代数与空间解析几何", 3, 4);
    }

    @org.junit.Test
    public void testSelectCourseList() throws Exception {
        CourseDO course = new CourseDO();
        course.setCredit(2);
        List<CourseDO> courses = dao.selectCourseList(course);
        System.out.println(course);
    }
}