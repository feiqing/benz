package com.fq.test.dao;

import com.fq.client.domain.ScoreDO;
import com.fq.core.dao.ScoreDAO;
import com.fq.test.TestBase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author jifang.
 * @since 2016/5/24 8:52.
 */
public class ScoreDAOTest extends TestBase {

    @Autowired
    private ScoreDAO dao;

    @Test
    public void selectStudentList() {
        List<ScoreDO> sutdents = dao.selectStudentList(20);
        System.out.println(sutdents);
    }

    @Test
    public void selectStudentTopN() {
        List<ScoreDO> student = dao.selectStudentTopN(19, 2);
        System.out.println(student);
    }

    @Test
    public void selectStudentUnPassed() {
        List<ScoreDO> student = dao.selectStudentUnPassed(19);
        System.out.println(student);
    }

    @Test
    public void selectScoreList() {
        List<ScoreDO> scores = dao.selectScoreList(1);
        System.out.println(scores);
    }

    @Test
    public void selectScoreTopN() {
        List<ScoreDO> scores = dao.selectScoreTopN(1, 5);
        System.out.println(scores);
    }

    @Test
    public void selectScoreUnPassed() {
        List<ScoreDO> scores = dao.selectScoreUnPassed(1);
        System.out.println(scores);
    }

    @Test
    public void selectScoreTotal() {
        Double total = dao.selectScoreTotal(1);
        System.out.println(total);
    }
}
