package com.fq.test.dao;


import com.fq.client.domain.TeacherDO;
import com.fq.core.dao.TeacherDAO;
import com.fq.core.util.PasswordUtil;
import com.fq.test.TestBase;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author jifang
 * @since 16/2/29 下午4:05.
 */
public class TeacherDODAOTest extends TestBase {

    @Autowired
    private TeacherDAO dao;

    @org.junit.Test
    public void testInsertTeacher() throws Exception {
        dao.insertTeacher(new TeacherDO(null, "丁长青", PasswordUtil.encodeByMd5("123")));
        dao.insertTeacher(new TeacherDO(null, "崔焕庆", PasswordUtil.encodeByMd5("123")));
    }

    @org.junit.Test
    public void selectTeacherByName() throws Exception {
        TeacherDO teacher = dao.selectTeacherByName("xin_teacher");
        System.out.println(teacher);
    }
}