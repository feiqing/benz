package com.fq.test.cache;

import com.fq.core.cache.RedisDAO;
import com.fq.test.TestBase;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * @author jifang
 * @since 16/3/1 上午9:12.
 */
public class RedisDAOTest extends TestBase {

    @Autowired
    private RedisDAO dao;

    @org.junit.Test
    public void testGet() throws Exception {
        Map<String, String> map = dao.get("1");
        System.out.println(map);
    }

    @org.junit.Test
    public void testPut() throws Exception {
        dao.put("1", "朱吉芳2", "老师, 我的编译原理成绩错了!");
        dao.put("1", "朱吉芳3", "老师, 我的编译原理成绩错了!");
    }
}