package com.fq.client.service;

import java.util.Map;

/**
 * @author jifang
 * @since 16/3/1 上午9:56.
 */
public interface MessageService {

    void sendMsg(String message, String uId, String tId);

    Map<String, String> recvMsg(String tId);

    Long getNewMsgCount(String tId);
}
