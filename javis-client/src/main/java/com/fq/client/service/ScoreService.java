package com.fq.client.service;

import com.fq.client.domain.ScoreDO;

import java.util.List;

/**
 * @author jifang.
 * @since 2016/5/24 9:10.
 */
public interface ScoreService {

    /**
     * 教师端
     **/
    List<ScoreDO> getStudentList(Integer cId);

    List<ScoreDO> getStudentTopN(Integer cId, Integer n);

    List<ScoreDO> getStudentUnPassed(Integer cId);

    /**
     * 学生端
     **/
    List<ScoreDO> getScoreList(Integer uId);

    List<ScoreDO> getScoreTopN(Integer uId, Integer n);

    List<ScoreDO> getScoreUnPassed(Integer uId);

    Double getScoreTotal(Integer uId);
}
