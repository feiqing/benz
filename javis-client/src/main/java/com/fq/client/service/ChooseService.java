package com.fq.client.service;

/**
 * @author jifang
 * @since 16/3/1 下午5:35.
 */
public interface ChooseService {

    Integer addChoose(Integer uId, Integer cId);

    void cancelChoose(Integer uId, Integer cId);

    void updateScore(Integer uId, Integer cId, Double number);

    boolean isAdded(Integer uId, Integer cId);
}
