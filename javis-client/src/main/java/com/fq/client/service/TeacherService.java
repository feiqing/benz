package com.fq.client.service;

/**
 * @author jifang
 * @since 15/12/16 下午5:09.
 */
public interface TeacherService {

    void register(String name, String password);

    Integer login(String name, String password);
}
