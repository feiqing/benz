package com.fq.client.service;

import com.fq.client.domain.CourseDO;

import java.util.List;

/**
 * @author jifang
 * @since 15/12/16 下午5:09.
 */
public interface CourseService {

    Integer addCourse(CourseDO course);

    List<CourseDO> getCourseByCredit(Integer credit);

    List<CourseDO> getCourseByTId(Integer tId);

    List<CourseDO> getAllCourse();
}
