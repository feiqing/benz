package com.fq.client.service;

import com.fq.client.domain.UserDO;

import java.util.List;

/**
 * @author jifang
 * @since 15/12/16 下午5:09.
 */
public interface UserService {

    Integer generate(String name, String password);

    boolean updatePasswordById(Integer id, String oldPassword, String newPassword);

    void removeUserById(Integer id);

    List<UserDO> getAllUsers();

    Integer login(Integer id, String password);
}
