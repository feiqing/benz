package com.fq.client.domain;


import java.io.Serializable;

/**
 * @author jifang
 * @since 16/2/29 上午10:20.
 */
public class CourseDO implements Serializable {

    private static final long serialVersionUID = 2625655110584180267L;

    private Integer id;

    private String name;

    private Integer credit;

    private Integer type;

    private String extension;

    private TeacherDO teacher;

    private Integer tId;

    public CourseDO() {
    }

    public CourseDO(Integer id, String name, Integer credit) {
        this.id = id;
        this.name = name;
        this.credit = credit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public TeacherDO getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDO teacher) {
        this.teacher = teacher;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }
}
