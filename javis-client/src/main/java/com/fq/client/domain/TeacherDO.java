package com.fq.client.domain;

import java.io.Serializable;

/**
 * @author jifang
 * @since 16/2/29 上午10:17.
 */
public class TeacherDO implements Serializable {

    private static final long serialVersionUID = 7505668982944112714L;

    private Integer id;

    private String name;

    private String password;

    private Integer type;

    private String extension;

    public TeacherDO() {
    }

    public TeacherDO(Integer id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
