package com.fq.client.domain;


import java.io.Serializable;

/**
 * @author jifang
 * @since 15/12/16 下午5:09.
 */
public class UserDO implements Serializable {

    private static final long serialVersionUID = 648028785096792326L;

    private Integer id;

    private String name;

    private String password;

    private Integer type;

    private String extension;

    private Double number;

    public UserDO() {
    }

    public UserDO(Integer id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }
}
