package com.fq.client.domain;

import java.io.Serializable;

/**
 * @author jifang
 * @since 16/2/29 上午10:19.
 */
public class ChooseDO implements Serializable {

    private static final long serialVersionUID = 1323036413414326744L;

    private Integer id;

    private Integer uId;

    private Integer cId;

    private Double number;

    private CourseDO course;

    private UserDO user;

    private Integer type;

    private String extension;

    public ChooseDO() {
    }

    public ChooseDO(Integer id, Double number, Integer uId, Integer cId) {
        this.id = id;
        this.number = number;
        this.uId = uId;
        this.cId = cId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public CourseDO getCourse() {
        return course;
    }

    public void setCourse(CourseDO course) {
        this.course = course;
    }

    public UserDO getUser() {
        return user;
    }

    public void setUser(UserDO user) {
        this.user = user;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
