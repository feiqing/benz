package com.fq.core.service.impl;

import com.fq.client.domain.ScoreDO;
import com.fq.client.service.ScoreService;
import com.fq.core.dao.ScoreDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jifang.
 * @since 2016/5/24 9:11.
 */
@Service
public class ScoreServiceImpl implements ScoreService {

    @Autowired
    private ScoreDAO dao;

    @Override
    public List<ScoreDO> getStudentList(Integer cId) {
        return dao.selectStudentList(cId);
    }

    @Override
    public List<ScoreDO> getStudentTopN(Integer cId, Integer n) {
        return dao.selectStudentTopN(cId, n);
    }

    @Override
    public List<ScoreDO> getStudentUnPassed(Integer cId) {
        return dao.selectStudentUnPassed(cId);
    }

    @Override
    public List<ScoreDO> getScoreList(Integer uId) {
        return dao.selectScoreList(uId);
    }

    @Override
    public List<ScoreDO> getScoreTopN(Integer uId, Integer n) {
        return dao.selectScoreTopN(uId, n);
    }

    @Override
    public List<ScoreDO> getScoreUnPassed(Integer uId) {
        return dao.selectScoreUnPassed(uId);
    }

    @Override
    public Double getScoreTotal(Integer uId) {
        return dao.selectScoreTotal(uId);
    }
}
