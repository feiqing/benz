package com.fq.core.service.impl;

import com.fq.client.domain.ChooseDO;
import com.fq.client.service.ChooseService;
import com.fq.core.dao.ChooseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jifang
 * @since 16/3/1 下午5:36.
 */
@Service
public class ChooseServiceImpl implements ChooseService {

    @Autowired
    private ChooseDAO dao;

    @Override
    public Integer addChoose(Integer uId, Integer cId) {
        ChooseDO choose = new ChooseDO();
        choose.setuId(uId);
        choose.setcId(cId);
        dao.insertChoose(choose);
        return choose.getId();
    }

    @Override
    public void cancelChoose(Integer uId, Integer cId) {
        ChooseDO choose = new ChooseDO();
        choose.setuId(uId);
        choose.setcId(cId);
        dao.deleteChoose(choose);
    }

    @Override
    public void updateScore(Integer uId, Integer cId, Double number) {
        ChooseDO choose = new ChooseDO();
        choose.setuId(uId);
        choose.setcId(cId);
        choose.setNumber(number);
        dao.updateScore(choose);
    }

    @Override
    public boolean isAdded(Integer uId, Integer cId) {
        List<ChooseDO> chooses = dao.selectChooseByUidCid(uId, cId);
        if (chooses == null || chooses.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
