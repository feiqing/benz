package com.fq.core.service.impl;

import com.fq.client.domain.TeacherDO;
import com.fq.client.service.TeacherService;
import com.fq.core.dao.TeacherDAO;
import com.fq.core.util.PasswordUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jifang
 * @since 15/12/16 下午5:10.
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherServiceImpl.class);

    @Autowired
    private TeacherDAO dao;

    @Override
    public void register(String name, String password) {
        dao.insertTeacher(new TeacherDO(null, name, PasswordUtil.encodeByMd5(password)));
    }

    @Override
    public Integer login(String name, String password) {
        TeacherDO teacher = dao.selectTeacherByName(name);
        if (teacher != null && PasswordUtil.checkEqual(password, teacher.getPassword())) {
            return teacher.getId();
        }
        return null;
    }
}
