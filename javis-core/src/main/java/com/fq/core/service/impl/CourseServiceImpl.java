package com.fq.core.service.impl;

import com.fq.client.domain.CourseDO;
import com.fq.client.service.CourseService;
import com.fq.core.dao.CourseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jifang
 * @since 16/2/29 下午5:39.
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDAO dao;

    @Override
    public Integer addCourse(CourseDO course) {
        dao.insertCourse(course);
        return course.getId();
    }

    @Override
    public List<CourseDO> getCourseByCredit(Integer credit) {
        return dao.selectCourseList(new CourseDO(null, null, credit));
    }

    @Override
    public List<CourseDO> getCourseByTId(Integer tId) {
        CourseDO course = new CourseDO();
        course.settId(tId);
        return dao.selectCourseList(course);
    }

    @Override
    public List<CourseDO> getAllCourse() {
        return dao.selectCourseList(new CourseDO());
    }
}
