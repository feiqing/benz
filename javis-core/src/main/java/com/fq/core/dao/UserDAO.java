package com.fq.core.dao;


import com.fq.client.domain.UserDO;

import java.util.Collection;
import java.util.List;

/**
 * @author jifang
 * @since 15/12/16 下午5:11.
 */
public interface UserDAO {

    void insertUser(UserDO user);

    void updatePasswordById(Integer id, String password);

    void deleteUserById(Integer id);

    UserDO selectUserById(Integer id);

    List<UserDO> selectUserByCId(Integer cId);

    List<UserDO> selectAllUsers();

    List<UserDO> selectUserByIds(Collection<Integer> ids);
}
