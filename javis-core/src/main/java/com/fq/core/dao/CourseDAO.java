package com.fq.core.dao;

import com.fq.client.domain.CourseDO;

import java.util.List;

/**
 * @author jifang
 * @since 16/2/29 下午4:00.
 */
public interface CourseDAO {

    void insertCourse(CourseDO course);

    List<CourseDO> selectCourseList(CourseDO course);
}
