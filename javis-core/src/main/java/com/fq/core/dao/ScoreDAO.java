package com.fq.core.dao;

import com.fq.client.domain.ScoreDO;

import java.util.List;

/**
 * @author jifang.
 * @since 2016/5/24 8:43.
 */
public interface ScoreDAO {

    /**
     * 教师端
     **/
    List<ScoreDO> selectStudentList(Integer cId);

    List<ScoreDO> selectStudentTopN(Integer cId, Integer n);

    List<ScoreDO> selectStudentUnPassed(Integer cId);

    /**
     * 学生端
     **/
    List<ScoreDO> selectScoreList(Integer uId);

    List<ScoreDO> selectScoreTopN(Integer uId, Integer n);

    List<ScoreDO> selectScoreUnPassed(Integer uId);

    Double selectScoreTotal(Integer uId);
}
