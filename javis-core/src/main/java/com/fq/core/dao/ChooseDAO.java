package com.fq.core.dao;

import com.fq.client.domain.ChooseDO;

import java.util.List;

/**
 * @author jifang
 * @since 16/3/1 下午4:17.
 */
public interface ChooseDAO {

    void insertChoose(ChooseDO choose);

    void deleteChoose(ChooseDO choose);

    void updateScore(ChooseDO choose);

    List<ChooseDO> selectChooseByUidCid(Integer uId, Integer cId);
}
