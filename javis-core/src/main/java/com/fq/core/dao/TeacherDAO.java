package com.fq.core.dao;

import com.fq.client.domain.TeacherDO;

/**
 * @author jifang
 * @since 16/2/29 下午4:00.
 */
public interface TeacherDAO {

    void insertTeacher(TeacherDO teacher);

    TeacherDO selectTeacherByName(String name);
}
