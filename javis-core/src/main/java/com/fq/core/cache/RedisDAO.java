package com.fq.core.cache;

import java.util.Map;

/**
 * @author jifang
 * @since 16/3/1 上午9:10.
 */
public interface RedisDAO {

    Map<String, String> get(String key);

    void put(String key, String field, String value);

    Long count(String key);
}
