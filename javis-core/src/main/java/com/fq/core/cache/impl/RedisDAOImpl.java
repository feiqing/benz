package com.fq.core.cache.impl;

import com.fq.core.cache.RedisDAO;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;

import java.util.Map;

/**
 * @author jifang
 * @since 16/3/1 上午8:55.
 */
@Repository
public class RedisDAOImpl implements RedisDAO {

    @Autowired
    private Jedis jedis;

    @Override
    public Map<String, String> get(String key) {
        if (jedis.exists(key)) {
            Map<String, String> result = jedis.hgetAll(key);
            jedis.del(key);
            return result;
        }
        return Maps.newHashMap();
    }

    @Override
    public void put(String key, String field, String value) {
        jedis.hset(key, field, value);
    }

    @Override
    public Long count(String key) {
        return jedis.hlen(key);
    }
}
